#include "Scene.h"



Scene::Scene(sf::RenderWindow *window)
{
	p_window = window;
}


Scene::~Scene()
{
}

void Scene::createChessboard() {

	chessboardTexture.loadFromFile("plansza.png");
	sf::Texture mainTextureWhite;
	mainTextureWhite.loadFromFile("pole_biale.png");
	sf::Texture selectedTextureWhite;
	selectedTextureWhite.loadFromFile("pole_biale_selected.png");
	sf::Texture mainTextureBlack;
	mainTextureBlack.loadFromFile("pole_czarne.png");
	sf::Texture selectedTextureBlack;
	selectedTextureBlack.loadFromFile("pole_czarne_selected.png");

	sf::Vector2f fieldSize(100, 100);
	sf::Vector2f fieldPosition(25, 25);


	for (int i = 0; i < chessSize; i++) {
		fieldPosition.x = 25;
		for (int j = 0; j < chessSize; j++) {
			if ((j+i) % 2 == 0) {
				chessboard[i][j].create(p_window,mainTextureWhite, selectedTextureWhite, selectedTextureWhite, fieldPosition, fieldSize);
			}
			else {
				chessboard[i][j].create(p_window, mainTextureBlack, selectedTextureBlack, selectedTextureBlack, fieldPosition, fieldSize);
				
			}
			fieldPosition.x = fieldPosition.x + fieldSize.x;
		}
		fieldPosition.y = fieldPosition.y + fieldSize.y;
	}
}

void Scene::drawChessboard() {

	sf::Sprite background;
	background.setTexture(chessboardTexture);
	p_window->draw(background);

	for (int i = 0; i < chessSize; i++) {
		for (int j = 0; j < chessSize; j++) {
			chessboard[i][j].SetProperties();
			chessboard[i][j].updateTextures();
			chessboard[i][j].draw();
		}
	}	
}


void Scene::createFigures() {

	sf::Vector2f fieldSize(100, 100);
	sf::Vector2f figureSize(80, 80);
	sf::Vector2f figurePosition(135, 35);

	sf::Texture mainTextureRed;
	mainTextureRed.loadFromFile("pionek_red.png");
	sf::Texture selectedTextureRed;
	selectedTextureRed.loadFromFile("pionek_wybrany_red.png");
	sf::Texture pressedTextureRed;
	pressedTextureRed.loadFromFile("pionek_wcisniety_red.png");

	sf::Texture mainTextureBlack;
	mainTextureBlack.loadFromFile("pionek_black.png");
	sf::Texture selectedTextureBlack;
	selectedTextureBlack.loadFromFile("pionek_wybrany_black.png");
	sf::Texture pressedTextureBlack;
	pressedTextureBlack.loadFromFile("pionek_wcisniety_black.png");

	int num = 0;
	for (int y = 0; y < 3; y++) {
		figurePosition.x = 35;
		for (int x = 0; x < chessSize; x++) {
			if ((y + x) % 2 == 1) {
logicChessboard[y][x] = RED;
figureRed[num].create(p_window, mainTextureRed, selectedTextureRed, pressedTextureRed, figurePosition, figureSize);
num++;
			}
			figurePosition.x = figurePosition.x + fieldSize.x;
		}
		figurePosition.y = figurePosition.y + fieldSize.y;
	}

	num = 0;
	figurePosition.y = 535;
	for (int y = 5; y < 8; y++) {
		figurePosition.x = 35;
		for (int x = 0; x < chessSize; x++) {
			if ((y + x) % 2 == 1) {
				logicChessboard[y][x] = BLACK;
				figureBlack[num].create(p_window, mainTextureBlack, selectedTextureBlack, pressedTextureBlack, figurePosition, figureSize);
				num++;
			}
			figurePosition.x = figurePosition.x + fieldSize.x;
		}
		figurePosition.y = figurePosition.y + fieldSize.y;
	}

}

void Scene::drawFigures() {

	for (int i = 0; i < figureQuantity; i++) {
		if (figureRed[i].getAcive() == 1) {
			figureRed[i].SetProperties();
			figureRed[i].updateTextures();
			figureRed[i].draw();
		}
		if (figureBlack[i].getAcive() == 1) {
			figureBlack[i].SetProperties();
			figureBlack[i].updateTextures();
			figureBlack[i].draw();
		}
	}
}

void Scene::writeLogicalTable() {
	std::cout << std::endl;
	for (int y = 0; y < chessSize; y++) {
		for (int x = 0; x < chessSize; x++) {
			std::cout << logicChessboard[y][x] << " ";
		}
		std::cout << std::endl;
	}


}

void Scene::setActiveFigure() {
	for (int i = 0; i < figureQuantity; i++) {
		if (figureRed[i].getPressDown() == 1 && figureRed[i].getAcive() == 1) {
			activeFigureRed = i;
			activeFigureBlack = -1;
		}
		if (figureBlack[i].getPressDown() == 1 && figureBlack[i].getAcive() == 1) {
			activeFigureBlack = i;
			activeFigureRed = -1;
		}
	}

}

bool Scene::isBeating(int color) {

	if (color == RED) {
		for (int i = 0; i < figureQuantity; i++) {
			if (figureRed[i].getAcive() == true) {
				bool beat = isFigureBeating(figureRed[i], RED);
				if (beat == true) return true;
			}
		}
	}
	if (color == BLACK) {
		for (int i = 0; i < figureQuantity; i++) {
			if (figureBlack[i].getAcive() == true) {
				bool beat = isFigureBeating(figureBlack[i], BLACK);
				if (beat == true) return true;
			}
		}
	}
	return false;
}

bool Scene::isFigureBeating(Figure &figure, int color) {

	int X = (figure.getPosition().x - 35) / 100;
	int Y = (figure.getPosition().y - 35) / 100;

	if (color == RED) {
		if ((Y + 2 < chessSize) && (X + 2 < chessSize))
			if (logicChessboard[Y + 1][X + 1] == BLACK && logicChessboard[Y + 2][X + 2] == EMPTY ) return true;
		if ((Y + 2 < chessSize) && (X - 2 >= 0))
			if (logicChessboard[Y + 1][X - 1] == BLACK && logicChessboard[Y + 2][X - 2] == EMPTY) return true;
		if ((Y - 2 >= 0) && (X + 2 < chessSize))
			if (logicChessboard[Y - 1][X + 1] == BLACK && logicChessboard[Y - 2][X + 2] == EMPTY) return true;
		if ((Y - 2 >= 0) && (X - 2 >= 0))
			if (logicChessboard[Y - 1][X - 1] == BLACK && logicChessboard[Y - 2][X - 2] == EMPTY) return true;
	}
	else if (color == BLACK) {
		if ((Y + 2 < chessSize) && (X + 2 < chessSize))
			if (logicChessboard[Y + 1][X + 1] == RED && logicChessboard[Y + 2][X + 2] == EMPTY) return true;
		if ((Y + 2 < chessSize) && (X - 2 >= 0))
			if (logicChessboard[Y + 1][X - 1] == RED && logicChessboard[Y + 2][X - 2] == EMPTY) return true;
		if ((Y - 2 >= 0) && (X + 2 < chessSize))
			if (logicChessboard[Y - 1][X + 1] == RED && logicChessboard[Y - 2][X + 2] == EMPTY) return true;
		if ((Y - 2 >= 0) && (X - 2 >= 0))
			if (logicChessboard[Y - 1][X - 1] == RED && logicChessboard[Y - 2][X - 2] == EMPTY) return true;
	}
	return false;

}

bool Scene::beatingTheFigure(int index_X, int index_Y, int color) {

	int second;
	int first = color;
	if (color == RED) {
		second = BLACK;
	}
	else if (color == BLACK) {
		second = RED;
	}
	std::cout << "beating: " << std::endl;
	for (int y = 0; y < chessSize; y++)
		for (int x = 0; x < chessSize; x++) {
			if (chessboard[y][x].getPressDown() == 1 && !(x == index_X && y == index_Y)) {
				if (y == index_Y + 2 && x == index_X + 2 && logicChessboard[y][x] == EMPTY && logicChessboard[index_Y + 1][index_X + 1] == second) {
					if (color == RED) {
						figureRed[activeFigureRed].move(x, y);
						for (int i = 0; i < figureQuantity; i++) {
							if (((figureBlack[i].getPosition().x - 35) / 100 == (index_X + 1)) && ((figureBlack[i].getPosition().y - 35) / 100 == (index_Y + 1))) {
								figureBlack[i].setActive(false);
							}
						}
					}
					else if (color == BLACK) {
						figureBlack[activeFigureBlack].move(x, y);
						for (int i = 0; i < figureQuantity; i++) {
							if (((figureRed[i].getPosition().x - 35) / 100 == (index_X + 1)) && ((figureRed[i].getPosition().y - 35) / 100 == (index_Y + 1))) {
								figureRed[i].setActive(false);
							}
						}
					}
					
					logicChessboard[index_Y][index_X] = EMPTY;
					logicChessboard[index_Y + 1][index_X + 1] = EMPTY;
					logicChessboard[y][x] = first;
					this->writeLogicalTable(); //
					return true;
				}
				else if (y == index_Y + 2 && x == index_X - 2 && logicChessboard[y][x] == EMPTY && logicChessboard[index_Y + 1][index_X - 1] == second) {
					if (color == RED) {
						figureRed[activeFigureRed].move(x, y);
						for (int i = 0; i < figureQuantity; i++) {
							if (((figureBlack[i].getPosition().x - 35) / 100 == (index_X - 1)) && ((figureBlack[i].getPosition().y - 35) / 100 == (index_Y + 1))) {
								figureBlack[i].setActive(false);
							}
						}
					}
					else if (color == BLACK) {
						figureBlack[activeFigureBlack].move(x, y);
						for (int i = 0; i < figureQuantity; i++) {
							if (((figureRed[i].getPosition().x - 35) / 100 == (index_X - 1)) && ((figureRed[i].getPosition().y - 35) / 100 == (index_Y + 1))) {
								figureRed[i].setActive(false);
							}
						}
					}
					
					logicChessboard[index_Y][index_X] = EMPTY;
					logicChessboard[index_Y + 1][index_X - 1] = EMPTY;
					logicChessboard[y][x] = first;
					this->writeLogicalTable(); //
					return true;
				}
				else if (y == index_Y - 2 && x == index_X + 2 && logicChessboard[y][x] == EMPTY && logicChessboard[index_Y - 1][index_X + 1] == second) {
					if (color == RED) {
						figureRed[activeFigureRed].move(x, y);
						for (int i = 0; i < figureQuantity; i++) {
							if (((figureBlack[i].getPosition().x - 35) / 100 == (index_X + 1)) && ((figureBlack[i].getPosition().y - 35) / 100 == (index_Y - 1))) {
								figureBlack[i].setActive(false);
							}
						}
					}
					else if (color == BLACK) {
						figureBlack[activeFigureBlack].move(x, y);
						for (int i = 0; i < figureQuantity; i++) {
							if (((figureRed[i].getPosition().x - 35) / 100 == (index_X + 1)) && ((figureRed[i].getPosition().y - 35) / 100 == (index_Y - 1))) {
								figureRed[i].setActive(false);
							}
						}
					}
					logicChessboard[index_Y][index_X] = EMPTY;
					logicChessboard[index_Y - 1][index_X + 1] = EMPTY;
					logicChessboard[y][x] = first;
					this->writeLogicalTable(); //
					return true;
				}
				else if (y == index_Y - 2 && x == index_X - 2 && logicChessboard[y][x] == EMPTY && logicChessboard[index_Y - 1][index_X - 1] == second) {
					if (color == RED) {
						figureRed[activeFigureRed].move(x, y);
						for (int i = 0; i < figureQuantity; i++) {
							if (((figureBlack[i].getPosition().x - 35) / 100 == (index_X - 1)) && ((figureBlack[i].getPosition().y - 35) / 100 == (index_Y - 1))) {
								figureBlack[i].setActive(false);
							}
						}
					}
					else if (color == BLACK) {
						figureBlack[activeFigureBlack].move(x, y);
						for (int i = 0; i < figureQuantity; i++) {
							if (((figureRed[i].getPosition().x - 35) / 100 == (index_X - 1)) && ((figureRed[i].getPosition().y - 35) / 100 == (index_Y - 1))) {
								figureRed[i].setActive(false);
							}
						}
					}
					logicChessboard[index_Y][index_X] = EMPTY;
					logicChessboard[index_Y - 1][index_X - 1] = EMPTY;
					logicChessboard[y][x] = first;
					this->writeLogicalTable(); //
					return true;
				}
			}
		}
	 return false;
}			


bool Scene::moveForward(int index_X, int index_Y, int color) {
	if (color == RED) {
		for (int y = 0; y < chessSize; y++)
			for (int x = 0; x < chessSize; x++) {
				if (chessboard[y][x].getPressDown() == 1 && !(x == index_X && y == index_Y)) {
					if (((index_X - x) == (index_Y - y) || (index_X - x) == -(index_Y - y)) && ((index_Y - y) < 0)) {		//Tylko po skosie do przodu
						if ((index_X - x) < 2 && (index_X - x) > -2) {														//Nie wi�cej ni� 1 pole
							if (logicChessboard[y][x] == EMPTY) {
								figureRed[activeFigureRed].move(x, y);
								logicChessboard[index_Y][index_X] = EMPTY;
								logicChessboard[y][x] = RED;
								this->writeLogicalTable();
								return true;
						
							}
						}

					}
				}
			}
	}

	if (color == BLACK) {
		for (int y = 0; y < chessSize; y++)
			for (int x = 0; x < chessSize; x++) {

				if (chessboard[y][x].getPressDown() == 1 && !(x == index_X && y == index_Y)) {
					if (((index_X - x) == (index_Y - y) || (index_X - x) == -(index_Y - y)) && ((index_Y - y) > 0)) {		//Tylko po skosie do przodu
						if ((index_X - x) < 2 && (index_X - x) > -2) {														//Nie wi�cej ni� 1 pole
							if (logicChessboard[y][x] == EMPTY) {
								figureBlack[activeFigureBlack].move(x, y);
								logicChessboard[index_Y][index_X] = EMPTY;
								logicChessboard[y][x] = BLACK;
								this->writeLogicalTable();
								return true;
							}
						}																								
					}
				}
			}
	}
	return false;
}

bool Scene::playerMove(int color) {
	sf::Vector2f activeFigurePosition;

	
	for (int i = 0; i < figureQuantity; i++) {
		if (figureRed[i].getAcive() == 1) {
			figureRed[i].SetProperties();
		}
	}
	for (int i = 0; i < figureQuantity; i++) {
		if (figureBlack[i].getAcive() == 1) {
			figureBlack[i].SetProperties();
		}
	}
	for (int i = 0; i < chessSize; i++) {
		for (int j = 0; j < chessSize; j++) {
			chessboard[i][j].SetProperties();
		}
	}

	setActiveFigure();
	if (activeFigureRed == -1 && color == RED) {
		return false;
	}
	if (activeFigureBlack == -1 && color == BLACK) {
		return false;
	}

	if (color == RED) {

		activeFigurePosition = figureRed[activeFigureRed].getPosition();
		int index_X = (activeFigurePosition.x - 35) / 100;
		int index_Y = (activeFigurePosition.y - 35) / 100;
		if (combo > -1) {
		//	std::cout << "Rcombo: "<< combo << std::endl;
			if (combo == activeFigureRed) {
			//	std::cout << "Rcombo active: " << combo << std::endl;
				beatingTheFigure(index_X, index_Y, RED);						//Bicie
				if (isFigureBeating(figureRed[activeFigureRed], RED)) {
					combo = activeFigureRed;
					return false;
				}
				else {
					combo = -1;
					return true;
				}
			}
			else return false;

		}
		else {
			if (beating == 1) {
				std::cout << "hue2" << std::endl;
				if (isFigureBeating(figureRed[activeFigureRed], RED)) {
					std::cout << "hue3" << std::endl;
					if (beatingTheFigure(index_X, index_Y, RED)) {
						std::cout << "hue4" << std::endl;
						if (isFigureBeating(figureRed[activeFigureRed], RED)) {
							combo = activeFigureRed;
							return false;
						}
						else {
							combo = -1;
							return true;
						}
					}
					else return false;
				}
				else return false;
			}
			else {
				std::cout << "Rmove" << std::endl;
				return moveForward(index_X, index_Y, RED);
			}
		}
	}
	else if (color == BLACK) {

		activeFigurePosition = figureBlack[activeFigureBlack].getPosition();
		int index_X = (activeFigurePosition.x - 35) / 100;
		int index_Y = (activeFigurePosition.y - 35) / 100;

		if (combo > -1) {
			//std::cout << "Bcombo: " << combo << std::endl;
			if (combo == activeFigureBlack) {
			//	std::cout << "Ractivecombo: " << combo << std::endl;
				beatingTheFigure(index_X, index_Y, BLACK);						//Bicie
				if (isFigureBeating(figureBlack[activeFigureBlack], BLACK)) {
					combo = activeFigureBlack;
					return false;
				}
				else {
					combo = -1;
					return true;
				}
			}
			else return false;

		}
		else {
			if (beating == 1) {
				std::cout << "Bhue1" << std::endl;
				if (isFigureBeating(figureBlack[activeFigureBlack], BLACK)) {
					std::cout << "Bhue2" << std::endl;
					beatingTheFigure(index_X, index_Y, BLACK);
					std::cout << "Bhue3" << std::endl;
					if (isFigureBeating(figureBlack[activeFigureBlack], BLACK)) {
						combo = activeFigureBlack;
						return false;
					}
					else {
						combo = -1;
						return true;
					}
				}
				else return false;
			}
			else {
				std::cout << "Bmove" << std::endl;
				return moveForward(index_X, index_Y, BLACK);
			}
		}
	}
}

void Scene::multiPlayer() {

	this->drawChessboard();
	this->drawFigures();
	
	if (turn == RED) {
		beating = isBeating(RED);
		change = playerMove(RED);
		if (change == true) {
			turn = BLACK;
			change = false;
			beating = false;
		}
	}
	else if (turn == BLACK) {
		beating = isBeating(BLACK);
		change = playerMove(BLACK);
		if (change == true) {
			turn = RED;
			change = false;
			beating = false;
		}
	}
}

