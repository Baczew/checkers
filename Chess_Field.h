#pragma once
#include "Field.h"


class Chess_Field : public Field
{


public:
	Chess_Field();
	Chess_Field(sf::RenderWindow * window, sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize);
	virtual ~Chess_Field();
	void create(sf::RenderWindow * window, sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize);
	virtual void SetProperties();
};

