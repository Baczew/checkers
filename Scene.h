#pragma once
#include "Chess_Field.h"
#include "Figure.h"

const int chessSize = 8;
const int figureQuantity = 12;
enum figureNumber{EMPTY, RED, BLACK};
class Scene
{
	sf::RenderWindow *p_window;
	Chess_Field chessboard[chessSize][chessSize];
	int logicChessboard[chessSize][chessSize];
	sf::Texture chessboardTexture;
	Figure figureRed[figureQuantity];
	Figure figureBlack[figureQuantity];
	int activeFigureRed = -1;
	int activeFigureBlack = -1;
	int combo = -1;
	int beating = 0;
	bool change = 0;
	int turn = RED;

	void writeLogicalTable();

public:
	Scene(sf::RenderWindow *window);
	~Scene();
	void createChessboard();
	void drawChessboard();
	void createFigures();
	void drawFigures();
	void setActiveFigure();
	bool isBeating(int color);
	bool isFigureBeating(Figure &figure, int color);
	bool beatingTheFigure(int index_X, int index_Y, int color);
	bool moveForward(int index_X, int index_Y, int color);
	bool playerMove(int color);
	void multiPlayer();
	//void move();
};

