#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "Chess_Field.h"
#include "Scene.h"
#include "Menu.h"
#include <iostream>

using namespace std;

int main()
{

	sf::RenderWindow menuwindow(sf::VideoMode(400, 600, 32), "MENU");
	Menu menu(&menuwindow);
	menu.createButtons();


	while (menuwindow.isOpen())
	{
		sf::Event menuEvent;
		while (menuwindow.pollEvent(menuEvent))
		{
			if (menuEvent.type == sf::Event::Closed)
				menuwindow.close();

			if (menuEvent.type == sf::Event::KeyPressed && menuEvent.key.code == sf::Keyboard::Escape)
				menuwindow.close();

			if (menuEvent.type == sf::Event::MouseButtonPressed && menuEvent.mouseButton.button == sf::Mouse::Middle)
				menuwindow.close();

		}
		menuwindow.clear();
		menu.drawButton();
		menuwindow.display();
	}
	

	sf::RenderWindow chesswindow(sf::VideoMode(850, 850, 32), "CHESS");
	Scene scene(&chesswindow);
	scene.createChessboard();
	scene.createFigures();

	while (chesswindow.isOpen())
	{
		sf::Event chessEvent;
		while (chesswindow.pollEvent(chessEvent))
		{
			if (chessEvent.type == sf::Event::Closed)
				chesswindow.close();

			if (chessEvent.type == sf::Event::KeyPressed && chessEvent.key.code == sf::Keyboard::Escape)
				chesswindow.close();

			if (chessEvent.type == sf::Event::MouseButtonPressed && chessEvent.mouseButton.button == sf::Mouse::Middle)
				chesswindow.close();

		}
		chesswindow.clear();

		scene.multiPlayer();
		chesswindow.display();
		
	}
	return 0;
}


/*
bool zmiana = 0;
int x = 25;
int y = 25;
for (int j = 0; j < 8; j++) {
	for (int i = 0; i < 4; i++) {
		obrazek2[i + j * 4].setPosition(sf::Vector2f(x, y));
		x = x + 200;
	}
	x = 25;
	if (zmiana == 0) x = x + 100;
	y = y + 100;
	zmiana = !zmiana;
}

zmiana = 0;
x = 125;
y = 25;
for (int j = 0; j < 8; j++) {
	for (int i = 0; i < 4; i++) {
		obrazek3[i + j * 4].setPosition(sf::Vector2f(x, y));
		x = x + 200;
	}
	x = 125;
	if (zmiana == 0) x = x - 100;
	y = y + 100;
	zmiana = !zmiana;
}

oknoAplikacji.draw(obrazek);
for (int i = 0; i < 32; i++) {
	oknoAplikacji.draw(obrazek2[i]);
}
for (int i = 0; i < 32; i++) {
	oknoAplikacji.draw(obrazek3[i]);
}
oknoAplikacji.display();
	}

	*/