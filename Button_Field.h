#pragma once
#include "Field.h"
class Button_Field :public Field
{

public:
	Button_Field();
	Button_Field(sf::RenderWindow * window, sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize);
	virtual ~Button_Field();
	void SetProperties();
};

