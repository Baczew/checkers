#pragma once
#include <vector>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>

enum TextureNumber { mainTexture, selectedTexture, pressedTexture };
enum Status { activeField, inactiveField };

class Field
{
protected:
	sf::RenderWindow * p_window;						//Wsk na okienko w kt�rym pole wyst�pi
	unsigned int howManyTextures;						//Ile dodano tekstur
	sf::Vector2f position;								//Srodkowy punkt
	sf::Vector2f size;									//Wysokosc i szerokosc
	std::vector < sf::Texture >  texture;
	sf::Sprite sprite;									//Sprite z aktualnie za�adowan� tekstur�
	bool active=0;										//Pole jest aktywne
	bool pressedDown=0;									//Pole jest wcisniete
	bool selected=0;									//Najechano na pole

public:
	Field();
	Field(sf::RenderWindow * window, sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize);
	~Field();
	void create(sf::RenderWindow * window, sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize);
	void draw();
	void updateTextures();
	void SetProperties();
	sf::Vector2f getPosition();
	bool getAcive();
	bool getPressDown();
	bool getSelected();
	void setActive(bool state);
	void setSelected(bool state);
	void setPressDown(bool state);


};

