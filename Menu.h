#pragma once
#include "Button_Field.h"


const int buttonQuontiti = 5;
enum {SINGLE, MULTI, LOAD, CREATOR, QUICK};
class Menu
{
	sf::RenderWindow *p_window;
	sf::Texture backgroundTexture;
	Button_Field button[buttonQuontiti];


public:
	Menu(sf::RenderWindow *window);
	~Menu();
	void createButtons();
	void drawButton();
};

