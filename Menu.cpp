#include "Menu.h"



Menu::Menu(sf::RenderWindow *window)
{
	p_window = window;
}


Menu::~Menu()
{
}

void Menu::createButtons() {

	backgroundTexture.loadFromFile("menu_background.png");
	sf::Texture mainTextureButton;
	mainTextureButton.loadFromFile("button.png");
	sf::Texture selectedTextureButton;
	selectedTextureButton.loadFromFile("button_selected.png");
	sf::Texture pressedTextureButton;
	pressedTextureButton.loadFromFile("button_pressed.png");

	sf::Vector2f fieldSize(300, 80);
	sf::Vector2f fieldPosition(50, 100);


	for (int i = 0; i < buttonQuontiti; i++) {
		button[i].create(p_window, mainTextureButton, selectedTextureButton, pressedTextureButton, fieldPosition, fieldSize);
		fieldPosition.y = fieldPosition.y + fieldSize.y +20;
	}
}


void Menu::drawButton() {

	sf::Sprite background;
	background.setTexture(backgroundTexture);
	p_window->draw(background);

	for (int i = 0; i < buttonQuontiti; i++) {
	
			button[i].SetProperties();
			button[i].updateTextures();
			button[i].draw();
	}
}
