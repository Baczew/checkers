#pragma once
#include "Field.h"

class Figure : public Field
{
public: 
	Figure();
	Figure(sf::RenderWindow * window, sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize);
	virtual ~Figure();
	void create(sf::RenderWindow * window, sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize);
	virtual void SetProperties();
	void move(int index_X, int index_Y);
};

