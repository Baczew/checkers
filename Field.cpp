#include "Field.h"


Field::Field() {}

Field::Field(sf::RenderWindow * window,sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize ){
	this->create( window,  textureBasic,  textureSelected, texturePressedDown, newPosition,  newSize);
}


Field::~Field()
{
	p_window = nullptr;
}

void Field::create(sf::RenderWindow * window, sf::Texture textureBasic, sf::Texture textureSelected, sf::Texture texturePressedDown, sf::Vector2f newPosition, sf::Vector2f newSize) {
	p_window = window;
	texture.push_back(textureBasic);
	texture.push_back(textureSelected);
	texture.push_back(texturePressedDown);
	howManyTextures = 3;
	position = newPosition;
	size = newSize;
	active = 1;

}

void Field::draw() {
	if (active == true) {
		sprite.setPosition(position);
		p_window->draw(sprite);
	}
}

void Field::updateTextures() {

	if (active == true) {
		if (selected == false && pressedDown == false) sprite.setTexture(texture[mainTexture]);
		if (selected == true && pressedDown == false) sprite.setTexture(texture[selectedTexture]);
		if (pressedDown == true) sprite.setTexture(texture[pressedTexture]);
	}
	else sprite.setTexture(texture[mainTexture]);
}

void Field::SetProperties() {
	if (active == true) {
		sf::Vector2i mousePosition = sf::Mouse::getPosition(*p_window);
		bool mousePressed = sf::Mouse::isButtonPressed(sf::Mouse::Left);

		if ((mousePosition.x > position.x) && (mousePosition.x < position.x + size.x) && (mousePosition.y > position.y) && (mousePosition.y < position.y + size.y)) {  //Poprawi� !!!!!!!!!!!!!!!!!!
			selected = true;

			if (mousePressed == true) {
				pressedDown = true;
			}
			if (pressedDown == true && mousePressed == false) {
				pressedDown = !pressedDown;
				pressedDown = false;
			}
		}
		else {
			selected = false;
			pressedDown = false;
		}
		std::cout << "Nie ta funkcja" << std::endl;
	}
}

sf::Vector2f Field::getPosition() {
	return position;
}

bool Field::getAcive() {
	return active;
}

bool Field::getPressDown(){
	return pressedDown;
}
bool Field::getSelected() {
	return selected;
}

void Field::setActive(bool state) {
	active = state;
}
void Field::setSelected(bool state) {
	selected = state;
}
void Field::setPressDown(bool state) {
	pressedDown = state;
}